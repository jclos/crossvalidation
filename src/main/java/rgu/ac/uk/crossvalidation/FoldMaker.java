package rgu.ac.uk.crossvalidation;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.function.Predicate;

/**
 *
 * @author Jérémie Clos <j.clos@rgu.ac.uk>
 */
public class FoldMaker {
    
    public static void main(String[] args) throws IOException {
        if (args.length == 5) {
           File data = new File(args[0]);
           File output = new File(args[1]);
           String separator = args[2];
           int classIndex = Integer.parseInt(args[3]);
           int k = Integer.parseInt(args[4]);
           Predicate<String> filter = (instance -> true);
           generate(data, output, separator, classIndex, k, filter);
        } else {
            System.out.println("Arguments required:");
            System.out.println("* dataFile: file containing the original data");
            System.out.println("* outputDirectory: directory where the folds will be stored - will be created if it doesn't exist");
            System.out.println("* separator: what character is used to separate the fields");
            System.out.println("* classIndex: at what index is the class");
            System.out.println("* k: number of folds");
        }
    } 
    
    
     /**
     * What it does:
     *  - given arguments k and file, generate 2k files: fold_i_train, fold_i_test
     *  - keeps the same distribution of classes as the original file
     * @param dataFile file containing the original data
     * @param outputDirectory directory where the folds will be stored - will be created if it doesn't exist
     * @param separator what character is used to separate the fields
     * @param classIndex at what index is the class
     * @param k number of folds
     * @throws java.io.IOException
     */
    public static void generate(File dataFile, File outputDirectory, String separator, int classIndex, int k) throws IOException {
        generate(dataFile, outputDirectory, separator, classIndex, k, instance -> true);
    }
    
    /**
     * What it does:
     *  - given arguments k and file, generate 2k files: fold_i_train, fold_i_test
     *  - keeps the same distribution of classes as the original file
     * @param dataFile file containing the original data
     * @param outputDirectory directory where the folds will be stored - will be created if it doesn't exist
     * @param separator what character is used to separate the fields
     * @param classIndex at what index is the class
     * @param k number of folds
     * @param labelFilter filters out instances whose label satisfies this predicate
     * @throws java.io.IOException
     */
    public static void generate(File dataFile, File outputDirectory, String separator, int classIndex, int k, Predicate<String> labelFilter) throws IOException {
        List<String> dataLines1 = Files.readAllLines(dataFile.toPath(), Charset.defaultCharset());
        List<String> dataLines = new ArrayList<>();
        Map<String, Integer> classes = new HashMap<>();
        for (String line : dataLines1) {
            String[] elements = line.split(separator);
            String classLabel =  elements[classIndex];
            if (labelFilter.test(classLabel)) {
                classes.put(classLabel, classes.getOrDefault(classLabel + 1, 0));
                dataLines.add(line);
            }
        }
        classes.keySet().stream().forEach(classLabel -> { System.out.println("[" + classLabel + " has " + classes.get(classLabel) + " instances]"); });
        Map<String, Double> ratios = new HashMap<>();
        classes.keySet().stream().forEach(classLabel -> {  
            ratios.put(classLabel, ((double) classes.get(classLabel)) / dataLines.size());
        });
        System.out.println("Ratios:");
        ratios.keySet().forEach(label -> System.out.println(label + ": " + ratios.get(label)));
        //estimate the size of each fold
        int foldSize = (int) Math.floor(dataLines.size() / k);
        int trainingSetSize = foldSize * (k - 1) ;
        int testingSetSize = foldSize ;
        System.out.println("[Size of training set: " + trainingSetSize + "]");
        System.out.println("[Size of testing set: " + testingSetSize + "]");
        List<Map<String, List<String>>> folds = new ArrayList<>();
        Random r = new Random();
        for (int i = 0 ; i < k ; i++) {
            Map<String, List<String>> fold = new HashMap<>();
            int currentSize = 0 ;
            while (currentSize < foldSize) {
                int index = r.nextInt(dataLines.size());
                String line = dataLines.get(index);
                String label = getLabel(line, classIndex, separator);
                int classLimit = (int) (foldSize * ratios.get(label));
                if (fold.containsKey(label)) {
                    if (fold.get(label).size() < (classLimit + 1)) {
                        fold.get(label).add(line);
                        currentSize++;
                        dataLines.remove(index);
                    }
                } else {
                    if (classLimit > 0) {
                        List<String> instances = new ArrayList<>();
                        instances.add(line);
                        fold.put(label, instances);
                        currentSize++ ;
                        dataLines.remove(index);
                    }
                }
            }
            folds.add(fold);
        }

        for (int i = 0; i < k; i++) {    
            Map<String, List<String>> testingSet = folds.get(i);
            Map<String, List<String>> trainingSet = new HashMap<>();
            for (int j = 0 ; j < k ; j++) if (i != j) {
                trainingSet = combine(trainingSet, folds.get(j));
            }
            File trainingFile = new File(outputDirectory.getAbsolutePath() + File.separator + "fold_" + (i+1) + "_training.csv");
            File testingFile = new File(outputDirectory.getAbsolutePath() + File.separator + "fold_" + (i+1) + "_testing.csv");
            StringBuilder trainingSetOutput = new StringBuilder();
            for (Map.Entry<String, List<String>> bla : trainingSet.entrySet()) {
                for (String line : bla.getValue()) {
                    trainingSetOutput.append(line).append("\n");
                }
            }
            System.out.println("[Writing in " + trainingFile.toPath() + "]");
            Files.write(trainingFile.toPath(), trainingSetOutput.toString().getBytes());
            StringBuilder testingSetOutput = new StringBuilder();
            for (Map.Entry<String, List<String>> bla : testingSet.entrySet()) {
                for (String line : bla.getValue()) {
                    testingSetOutput.append(line).append("\n");
                }
            }
            System.out.println("[Writing in " + testingFile.toPath() + "]");
            Files.write(testingFile.toPath(), testingSetOutput.toString().getBytes());
            
        }
        
    }
    
    public static Map<String, List<String>> combine(Map<String, List<String>> a, Map<String, List<String>> b) {
        Map<String, List<String>> output = new HashMap<>();
        Set<String> keys = new HashSet<>();
        keys.addAll(a.keySet());
        keys.addAll(b.keySet());
        for (String c : keys) {
            List<String> instancesA = a.getOrDefault(c, new ArrayList<>());
            List<String> instancesB = b.getOrDefault(c, new ArrayList<>());
            List<String> union = new ArrayList<>();
            union.addAll(instancesA);
            union.addAll(instancesB);
            output.put(c, union);
        }

        return output ;
    }
    
    public static String getLabel(String line, int index, String separator) {
        String[] elements = line.split(separator);
        return elements[index];
    }
}
